package com.projects.booking.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * If the room is already occupied when creating a new reservation
 */
@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class RoomOccupiedException extends RuntimeException {

    public RoomOccupiedException(String message) {
        super(message);
    }
}
