package com.projects.booking.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * If, when creating a new booking, the participant does not find it by Email
 */
@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class ParticipantNotFoundException extends RuntimeException {

    public ParticipantNotFoundException(String message) {
        super(message);
    }
}
