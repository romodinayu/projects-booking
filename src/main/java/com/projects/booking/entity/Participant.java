package com.projects.booking.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.util.Assert;

/**
 * Participant
 */
@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Participant implements Serializable {

    /**
     * ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Surname
     */
    @Column(nullable = false, length = 50)
    private String lastName;

    /**
     * Name
     */
    @Column(nullable = false, length = 50)
    private String firstName;

    /**
     * Login
     */
    @Transient
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @Column(nullable = false, length = 50, unique = true)
    private String login;

    /**
     * Email
     */
    @Column(nullable = false, length = 100, unique = true)
    private String email;

    /**
     * Password
     */
    @Transient
    @ToString.Exclude
    @Column(nullable = false)
    @EqualsAndHashCode.Exclude
    private String password;

    /**
     * User booking
     */
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(mappedBy = "participants", cascade = CascadeType.ALL)
    private Set<Booking> bookings = new HashSet<>();

    /**
     * Getting the full user name
     *
     * @return {@link String}
     * @throws IllegalArgumentException if the argument is null
     */
    public String getFullName() {
        Assert.hasText(firstName, "firstName is blank");
        Assert.hasText(lastName, "lastName is blank");

        return lastName + " " + firstName;
    }

    /**
     * Getting the short-full user name
     *
     * @return {@link String}
     * @throws IllegalArgumentException if the argument is null
     */
    public String getShortFullName() {
        Assert.hasText(firstName, "firstName is blank");
        Assert.hasText(lastName, "lastName is blank");

        return lastName + " " + firstName.charAt(0) + ".";
    }
}