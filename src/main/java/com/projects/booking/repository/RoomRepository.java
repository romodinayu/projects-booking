package com.projects.booking.repository;

import com.projects.booking.entity.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Access to the {@link Room} table
 */
@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {
}