package com.projects.booking.service.converter;

import java.util.stream.Collectors;

import com.projects.booking.entity.Booking;
import com.projects.booking.entity.Participant;
import com.projects.booking.service.booking.dto.HourlyEventData;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * {@link Booking} => {@link HourlyEventData.EventData}
 */
@Component
public class BookingToEventDataConverter
        implements Converter<Booking, HourlyEventData.EventData> {

    @Override
    public HourlyEventData.EventData convert(Booking entity) {

        return HourlyEventData.EventData.builder()
                .bookingId(entity.getId())
                .roomName(entity.getRoom().getName())
                .participants(
                        entity.getParticipants()
                                .stream()
                                .map(Participant::getShortFullName)
                                .collect(Collectors.toUnmodifiableList())
                )
                .build();
    }
}