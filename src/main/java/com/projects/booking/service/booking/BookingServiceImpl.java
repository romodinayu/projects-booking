package com.projects.booking.service.booking;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static java.time.DayOfWeek.MONDAY;
import static java.time.format.TextStyle.FULL_STANDALONE;
import static java.time.temporal.TemporalAdjusters.previousOrSame;
import com.google.api.services.calendar.model.Event;
import com.projects.booking.entity.Booking;
import com.projects.booking.entity.Participant;
import com.projects.booking.entity.Room;
import com.projects.booking.exception.BookingResourceNotFoundException;
import com.projects.booking.exception.ParticipantNotFoundException;
import com.projects.booking.exception.RoomOccupiedException;
import com.projects.booking.repository.BookingRepository;
import com.projects.booking.repository.ParticipantRepository;
import com.projects.booking.repository.RoomRepository;
import com.projects.booking.service.booking.dto.CreateBookingData;
import com.projects.booking.service.booking.dto.DayWeekData;
import com.projects.booking.service.booking.dto.HourlyEventData;
import com.projects.booking.web.rest.dto.response.BookingResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * Realization {@link BookingService}
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class BookingServiceImpl implements BookingService {

    private final Locale locale = new Locale("ru");
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    private final BookingRepository bookingRepository;
    private final ParticipantRepository participantRepository;
    private final RoomRepository roomRepository;

    private final ApplicationEventPublisher applicationEventPublisher;

    private final ConversionService conversionService;

    @Override
    public List<DayWeekData> getDaysWeek(final LocalDate currentDate) {
        Assert.notNull(currentDate, "currentDate = null");

        LocalDate monday = currentDate.with(previousOrSame(MONDAY));

        List<DayWeekData> weekData = new LinkedList<>();

        for (byte i = 0; i < 7; i++) {
            LocalDate dayOfWeek = monday.plusDays(i);
            weekData.add(
                    DayWeekData.builder()
                            .name(dayOfWeek.getDayOfWeek().getDisplayName(FULL_STANDALONE, locale))
                            .date(formatter.format(dayOfWeek))
                            .build()
            );
        }

        return weekData;
    }

    @Override
    public List<HourlyEventData> getHourlyEvents(final LocalDate currentDate) {
        Assert.notNull(currentDate, "currentDate = null");

        LocalDate mondayWeek = currentDate.with(previousOrSame(MONDAY));

        Map<Integer, Map<LocalDate, List<Booking>>> bookingsWeek = bookingRepository.findBookingsWeek(
                mondayWeek.atStartOfDay(),
                mondayWeek.plusWeeks(1).atStartOfDay()
        )
                .stream()
                .collect(Collectors.groupingBy(booking -> booking.getBeginDateTime().getHour(),
                        Collectors.groupingBy(booking -> booking.getBeginDateTime().toLocalDate())));

        List<HourlyEventData> events = new LinkedList<>();
        LocalTime time = LocalTime.of(0, 0);

        for (int h = 0; h < 24; h++) {
            List<List<HourlyEventData.EventData>> weekEvents = new LinkedList<>();

            for (AtomicInteger d = new AtomicInteger(0); d.get() < 7; d.incrementAndGet()) {
                List<HourlyEventData.EventData> dayEvents = new LinkedList<>();

                Optional.ofNullable(bookingsWeek.get(h))
                        .flatMap(bookings -> Optional.ofNullable(bookings.get(mondayWeek.plusDays(d.get()))))
                        .ifPresent(bookings -> dayEvents.addAll(bookings
                                .stream()
                                .sorted(Comparator.comparing(Booking::getBeginDateTime))
                                .map(booking -> conversionService.convert(booking, HourlyEventData.EventData.class))
                                .collect(Collectors.toUnmodifiableList())
                        ));

                weekEvents.add(dayEvents);
            }

            events.add(
                    HourlyEventData.builder()
                            .hour(time.plusHours(h).toString())
                            .events(weekEvents)
                            .build()
            );
        }
        return events;
    }

    @Override
    public BookingResponse getBooking(final Long id) {
        Assert.notNull(id, "id = null");

        return conversionService.convert(
                bookingRepository.findById(id)
                        .orElseThrow(() -> new BookingResourceNotFoundException(String.format("Booking(id = %d) not found", id))),
                BookingResponse.class
        );
    }

    @Override
    @Transactional
    public void createBooking(final CreateBookingData request) {
        Assert.notNull(request, "request = null");

        final Long roomId = request.getRoomId();
        final LocalDateTime beginDateTime = request.getBeginDateTime();
        final LocalDateTime endDateTime = request.getEndDateTime();

        final Room room = roomRepository.findById(roomId)
                .orElseGet(() -> {
                    final String msg = String.format("The room(id='%d') is not found", roomId);
                    log.error(msg);
                    throw new RoomOccupiedException(msg);
                });

        final boolean checkInteraction = bookingRepository
                .checkingIntersectionBookings(
                        beginDateTime,
                        endDateTime,
                        roomId
                );

        if (checkInteraction) {
            final String msg = String.format("The room(id='%d') is occupied", roomId);
            log.error(msg);
            throw new RoomOccupiedException(msg);
        }

        final Set<Participant> participants = new HashSet<>();
        request.getParticipantsOfBooking().forEach(email -> {
            final Participant participant = participantRepository
                    .findByEmail(email)
                    .orElseGet(() -> {
                        final String msg = String.format("The participant(email='%s') is not found", email);
                        log.error(msg);
                        throw new ParticipantNotFoundException(msg);
                    });

            participants.add(participant);
        });

        final Booking booking = Booking.builder()
                .beginDateTime(beginDateTime)
                .endDateTime(endDateTime)
                .duration(request.getDuration().intValue())
                .room(room)
                .participants(participants)
                .build();

        bookingRepository.save(booking);

        // push to google-calendar
        applicationEventPublisher.publishEvent(
                conversionService.convert(booking, Event.class)
        );
    }
}