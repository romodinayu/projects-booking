package com.projects.booking.service.booking.dto;

import java.time.LocalDateTime;
import java.util.Set;

import lombok.Builder;
import lombok.Data;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.util.Assert;

@Data
@Builder
public class CreateBookingData {

    /**
     * Room ID
     */
    private final Long roomId;

    /**
     * Start date+time
     */
    private final LocalDateTime beginDateTime;

    /**
     * Start date+time
     */
    private final LocalDateTime endDateTime;

    /**
     * Duration of booking
     */
    private final Long duration;

    /**
     * Participants
     */
    private final Set<String> participantsOfBooking;

    public static CreateBookingDataBuilder builder() {
        return new CreateBookingDataBuilder() {

            @Override
            public CreateBookingData build() {
                Assert.notNull(super.roomId, "roomId = null");
                Assert.notNull(super.beginDateTime, "beginDateTime = null");
                Assert.notNull(super.endDateTime, "endDateTime = null");
                Assert.isTrue(super.endDateTime.isAfter(super.beginDateTime), "beginDateTime isAfter endDateTime");
                Assert.notNull(super.duration, "duration = null");
                Assert.isTrue(super.duration >= 30 && super.duration <= 1440, "duration is not valid");
                Assert.notEmpty(super.participantsOfBooking, "participantsOfBooking is empty");
                super.participantsOfBooking.forEach(participant -> {
                            Assert.hasText(participant, "participant is blank");
                            Assert.isTrue(EmailValidator.getInstance().isValid(participant), "email is not valid");
                        }
                );

                return super.build();
            }
        };
    }
}