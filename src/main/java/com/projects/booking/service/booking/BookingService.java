package com.projects.booking.service.booking;

import java.time.LocalDate;
import java.util.List;

import com.projects.booking.exception.BookingResourceNotFoundException;
import com.projects.booking.exception.ParticipantNotFoundException;
import com.projects.booking.exception.RoomOccupiedException;
import com.projects.booking.service.booking.dto.CreateBookingData;
import com.projects.booking.service.booking.dto.DayWeekData;
import com.projects.booking.service.booking.dto.HourlyEventData;
import com.projects.booking.web.rest.dto.response.BookingResponse;

/**
 * Service logic for booking
 */
public interface BookingService {

    /**
     * Getting data for the header of a table with events
     *
     * @param currentDate {@link LocalDate}
     * @return list  {@link DayWeekData}
     * @throws IllegalArgumentException if the argument is null
     */
    List<DayWeekData> getDaysWeek(LocalDate currentDate);

    /**
     * Getting a list of events for the week
     *
     * @param currentDate {@link LocalDate}
     * @return list  {@link HourlyEventData}
     * @throws IllegalArgumentException if the argument is null
     */
    List<HourlyEventData> getHourlyEvents(LocalDate currentDate);

    /**
     * Getting booking parameters
     *
     * @param id {@link Long}
     * @return {@link BookingResponse}
     * @throws IllegalArgumentException         if the argument is null
     * @throws BookingResourceNotFoundException if the booking is not found
     */
    BookingResponse getBooking(Long id);

    /**
     * Add a new booking
     *
     * @param request {@link CreateBookingData}
     * @throws IllegalArgumentException     if the argument is null
     * @throws ParticipantNotFoundException if the participant is not found
     * @throws RoomOccupiedException        if the room is occupied
     */
    void createBooking(CreateBookingData request);
}