package com.projects.booking.service.booking.dto;

import lombok.Builder;
import lombok.Data;
import org.springframework.util.Assert;

/**
 * Parameters of the day of the week
 */
@Data
@Builder
public class DayWeekData {

    /**
     * Name
     */
    private final String name;

    /**
     * Date
     */
    private final String date;

    public static DayWeekDataBuilder builder() {
        return new DayWeekDataBuilder() {

            @Override
            public DayWeekData build() {
                Assert.hasText(super.name, "name is blank");
                Assert.hasText(super.date, "date is blank");

                return super.build();
            }
        };

    }
}
