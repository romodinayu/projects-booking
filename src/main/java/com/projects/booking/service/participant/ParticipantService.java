package com.projects.booking.service.participant;

import java.util.List;

/**
 * Service logic for processing participant data
 */
public interface ParticipantService {

    /**
     * Search for emails of participants
     *
     * @param request {@link String}
     * @return list {@link String}
     * @throws IllegalArgumentException if the argument is null
     */
    List<String> searchEmails(String request);
}