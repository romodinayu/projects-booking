package com.projects.booking.service.room;

import java.util.List;
import java.util.stream.Collectors;

import com.projects.booking.repository.RoomRepository;
import com.projects.booking.service.room.dto.RoomData;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

/**
 * Service logic for rooms
 */
@Service
@RequiredArgsConstructor
public class RoomService {

    private final RoomRepository roomRepository;

    private final ConversionService conversionService;

    public final List<RoomData> getRooms() {
        return roomRepository.findAll()
                .stream()
                .map(room -> conversionService.convert(room, RoomData.class))
                .collect(Collectors.toUnmodifiableList());
    }
}