package com.projects.booking.web.rest;

import javax.validation.Valid;
import java.util.List;

import static com.projects.booking.web.rest.ParticipantRestController.PARTICIPANT_PATH;
import com.projects.booking.service.participant.ParticipantService;
import com.projects.booking.web.rest.dto.request.ParticipantSearchRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Getting user data
 */
@RestController
@RequiredArgsConstructor
@RequestMapping(PARTICIPANT_PATH)
public class ParticipantRestController {

    public static final String PARTICIPANT_PATH = "/participants";
    private static final String SEARCH_PATH = "/search";

    private final ParticipantService participantService;

    @PostMapping(SEARCH_PATH)
    public List<String> searchParticipantEmail(@RequestBody @Valid final ParticipantSearchRequest request) {

        return participantService.searchEmails(request.getRequest());
    }
}