package com.projects.booking.web.rest.dto.response;

import java.util.List;

import lombok.Builder;
import lombok.Data;
import org.springframework.util.Assert;

/**
 * Parameters of booking
 */
@Data
@Builder
public class BookingResponse {

    /**
     * ID
     */
    private final Long id;

    /**
     * Name of the room
     */
    private final String roomName;

    /**
     * Start date+time
     */
    private final String beginDateTime;

    /**
     * End date+time
     */
    private final String endDateTime;

    /**
     * Duration of booking
     */
    private final Integer duration;

    /**
     * List of participants
     */
    private final List<Participant> participants;

    public static BookingResponseBuilder builder() {
        return new BookingResponseBuilder() {

            @Override
            public BookingResponse build() {
                Assert.notNull(super.id, "id = null");
                Assert.hasText(super.roomName, "roomName is blank");
                Assert.hasText(super.beginDateTime, "beginDateTime = null");
                Assert.hasText(super.endDateTime, "endDateTime = null");
                Assert.notNull(super.duration, "duration = null");
                Assert.notEmpty(super.participants, "participants = null");

                return super.build();
            }
        };
    }

    @Data
    @Builder
    public static class Participant {

        /**
         * Short-full name participant
         */
        private final String shortFullName;

        /**
         * Email
         */
        private final String email;

        public static ParticipantBuilder builder() {
            return new ParticipantBuilder() {

                @Override
                public Participant build() {
                    Assert.hasText(super.shortFullName, "shortFullName is blank");
                    Assert.hasText(super.email, "email is blank");

                    return super.build();
                }
            };
        }
    }
}