package com.projects.booking.web.rest.dto.request;

import javax.validation.constraints.NotBlank;

import lombok.Data;

/**
 * Data for searching emails of participants
 */
@Data
public class ParticipantSearchRequest {

    /**
     * Search request
     */
    @NotBlank(message = "Can't be empty")
    private final String request;
}
