package com.projects.booking.web.dto.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.Set;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

@Data
public class CreateBookingRequest {

    /**
     * Room ID
     */
    @NotNull(message = "Идентификатор комнаты не может быть пыстым")
    private Long roomId;

    /**
     * Start date+time
     */
    @NotNull(message = "Дата начала не может быть пустой")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime beginDateTime;

    /**
     * End date+time
     */
    @NotNull(message = "Дата окончания не может быть пустой")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime endDateTime;

    /**
     * Participants
     */
    @NotEmpty(message = "Список участников не может быть пустым")
    private Set<
            @NotBlank(message = "Email не может быть пустым")
            @Email(message = "Невалидный формат Email")
            @Size(min = 5, max = 320, message = "Email должен содержать от 5 до 320 символов")
                    String> participantsOfBooking;
}