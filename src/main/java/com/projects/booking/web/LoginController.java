package com.projects.booking.web;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Controller for auth
 */
@Controller
@RequiredArgsConstructor
public class LoginController {

    private static final String LOGIN_PAGE = "login";

    private static final String LOGIN_PATH = "/login";

    @GetMapping(LOGIN_PATH)
    public String getLoginPage() {

        return LOGIN_PAGE;
    }
}