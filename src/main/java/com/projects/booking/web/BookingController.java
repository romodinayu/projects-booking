package com.projects.booking.web;

import javax.validation.Valid;
import java.time.LocalDate;

import static java.time.DayOfWeek.MONDAY;
import static java.time.temporal.TemporalAdjusters.previousOrSame;
import static java.util.Objects.isNull;
import com.projects.booking.service.booking.BookingService;
import com.projects.booking.service.booking.dto.CreateBookingData;
import com.projects.booking.web.dto.request.CreateBookingRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.ConversionService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Controller for accessing the main page
 */
@Slf4j
@Controller
@RequiredArgsConstructor
public class BookingController {

    private static final String MAIN_PAGE = "index";
    private static final String BOOKING_PAGE = "booking";

    private static final String BOOKING_PATH = "/booking";
    private static final String CREATE_PATH = BOOKING_PATH + "/create";

    private final BookingService bookingService;

    private final ConversionService conversionService;

    @GetMapping()
    public String getMainPage(final @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam(required = false) LocalDate dayWeek,
                              final Model model) {

        LocalDate mondayCurrentWeek = (isNull(dayWeek)
                ? LocalDate.now()
                : dayWeek
        )
                .with(previousOrSame(MONDAY));

        model.addAttribute("week", bookingService.getDaysWeek(mondayCurrentWeek));
        model.addAttribute("events", bookingService.getHourlyEvents(mondayCurrentWeek));

        model.addAttribute("nextWeek", mondayCurrentWeek.plusWeeks(1));
        model.addAttribute("previousWeek", mondayCurrentWeek.minusWeeks(1));

        return MAIN_PAGE;
    }

    @GetMapping(BOOKING_PATH)
    public String getBookingPage(final Model model) {
        model.addAttribute("createBookingRequest", new CreateBookingRequest());

        return BOOKING_PAGE;
    }

    @PostMapping(CREATE_PATH)
    public String createBooking(final @ModelAttribute("createBookingRequest") @Valid CreateBookingRequest request,
                                final BindingResult bindingResult) {
        log.debug("REQUEST BookingController.createBooking(): {}", request);

        if (bindingResult.hasErrors()) {
            return BOOKING_PAGE;
        }

        final LocalDate currentMondayWeek = request
                .getBeginDateTime()
                .toLocalDate()
                .with(previousOrSame(MONDAY));

        bookingService.createBooking(conversionService.convert(request, CreateBookingData.class));

        return "redirect:/?dayWeek=" + currentMondayWeek;
    }
}